//
//  AddContactViewController.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/2/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"

@class MainViewController;
@class InformationViewController;

@interface AddContactViewController : UIViewController

//methods
- (void) fillInformationForPerson: (Person*) person
                              inPersonArray: (NSMutableArray*) personsArray;

- (void) fillContentWithTitleContactLogo: (NSString*) titleContactLogo;

@end
