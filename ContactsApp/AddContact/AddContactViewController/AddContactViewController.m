//
//  AddContactViewController.m
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/2/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AddContactViewController.h"
#import "MainViewController.h"
#import "CollectionMainViewController.h"
#import "InformationViewController.h"

@interface AddContactViewController () <UITextFieldDelegate, UIScrollViewDelegate>

//outlets
@property (weak, nonatomic) IBOutlet UITextField*   firstNameField;
@property (weak, nonatomic) IBOutlet UITextField*   lastNameField;
@property (weak, nonatomic) IBOutlet UITextField*   kindOfActivityField;
@property (weak, nonatomic) IBOutlet UITextField*   phoneNumberField;
@property (weak, nonatomic) IBOutlet UIButton*       readyButton;
@property (weak, nonatomic) IBOutlet UIScrollView* scrollView;
@property (weak, nonatomic) IBOutlet UIButton*       avatarButton;

//properties
@property (weak, nonatomic) CollectionMainViewController* collectionMainViewController;
@property (strong, nonatomic) NSMutableArray* personsArray;
@property (weak, nonatomic) Person* editingPerson;
@property (assign, nonatomic) NSInteger indexEditingPerson;
@property (weak, nonatomic) NSString* titleContactLogo;

@end

@implementation AddContactViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self configureAddContactViewController];
}

- (void) dealloc
{
    [self deregisterFromKeyboardNotifications];
}

#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn: (UITextField*) textField
{
    [self becomeFirstResponderForNextField: textField];
    
    return YES;
}


#pragma mark - Keyboard methods -

- (void) registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver: self
                                                                            selector: @selector(keyboardWasShown:)
                                                                                name: UIKeyboardDidShowNotification
                                                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                                                            selector: @selector(keyboardWillBeHidden:)
                                                                                name: UIKeyboardWillHideNotification
                                                                               object: nil];
}

- (void) deregisterFromKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                                                        name: UIKeyboardDidShowNotification
                                                                                      object: nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                                                       name: UIKeyboardWillHideNotification
                                                                                      object: nil];
}

- (void) keyboardWasShown: (NSNotification*) notification
{
    NSDictionary* info = [notification userInfo];
    
    CGSize keyboardSize = [[info objectForKey: UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGPoint buttonOrigin  = self.readyButton.frame.origin;
    CGFloat buttonHeight  = self.readyButton.frame.size.height;
    CGRect visibleRect      = self.view.frame;
    
    visibleRect.size.height -= keyboardSize.height + 70;
    
    if (!CGRectContainsPoint(visibleRect, buttonOrigin))
    {
        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight);
        
        [self.scrollView setContentOffset: scrollPoint
                                                animated: YES];
    }
}

- (void) keyboardWillBeHidden: (NSNotification*) notification
{
    [self.scrollView setContentOffset: CGPointZero
                                            animated: YES];
}


#pragma mark - Actions

- (IBAction) onReadyButtonPressed: (UIButton*) sender
{
    [self resignFirstResponderFromAnyField];
    
    if (self.editingPerson)
    {
        [self updatePersonInformationAndGoToTheMainController];
    }
    else
    {
        [self addNewRowAndGoToThePreviousController];
    }
}


#pragma mark - Public methods -

- (void) fillInformationForPerson: (Person*) person
                              inPersonArray: (NSMutableArray*) personsArray
{
    self.editingPerson = person;
    self.personsArray = personsArray;
}

- (void) fillContentWithTitleContactLogo: (NSString*) titleContactLogo
{
    self.editingPerson.titleContactLogo = titleContactLogo;
    self.titleContactLogo = titleContactLogo;
    
    UIImage* contactImage = [UIImage imageNamed: titleContactLogo];
    [self.avatarButton setImage: contactImage
                                     forState: UIControlStateNormal];

}

#pragma mark - Private methods -

- (void) configureAddContactViewController
{
    [self registerForKeyboardNotifications];
    [self configureAvatarImageView];
    
    if (self.editingPerson)
    {
        self.indexEditingPerson = [self.personsArray indexOfObject: self.editingPerson];
        
        self.firstNameField.text        = self.editingPerson.firstName;
        self.lastNameField.text         = self.editingPerson.lastName;
        self.kindOfActivityField.text = self.editingPerson.kindOfActivity;
        self.phoneNumberField.text = self.editingPerson.phoneNumber;
        
        UIImage* contactImage = [UIImage imageNamed: self.editingPerson.titleContactLogo];
        [self.avatarButton setImage: contactImage
                                         forState: UIControlStateNormal];
    }
}

- (void) configureAvatarImageView
{
        self.avatarButton.imageView.layer.backgroundColor = [[UIColor clearColor] CGColor];
        self.avatarButton.imageView.layer.cornerRadius        =  self.avatarButton.imageView.frame.size.height / 2;
        self.avatarButton.imageView.layer.masksToBounds   = YES;
        self.avatarButton.imageView.layer.borderColor          = [[UIColor blackColor] CGColor];
        self.avatarButton.imageView.layer.borderWidth         = 4.0;
        self.avatarButton.imageView.clipsToBounds               = YES;
}

- (void) resignFirstResponderFromAnyField
{
    if ([self.firstNameField isFirstResponder])
    {
        [self.firstNameField resignFirstResponder];
    }
    else if ([self.lastNameField isFirstResponder])
    {
        [self.lastNameField resignFirstResponder];
    }
    else if ([self.kindOfActivityField isFirstResponder])
    {
        [self.kindOfActivityField resignFirstResponder];
    }
    else if ([self.phoneNumberField isFirstResponder])
    {
        [self.phoneNumberField resignFirstResponder];
    }
}

- (void) becomeFirstResponderForNextField: (UITextField*) textField
{
    if ([textField isEqual: self.firstNameField])
    {
        [self.lastNameField becomeFirstResponder];
    }
    else if ([textField isEqual: self.lastNameField])
    {
        [self.kindOfActivityField becomeFirstResponder];
    }
    else if ([textField isEqual: self.kindOfActivityField])
    {
        [self.phoneNumberField becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
}

- (void) addNewRowAndGoToThePreviousController
{
    Person* newPerson = [self getPersonWithInformationFromFields];
    
    CollectionMainViewController* collectionViewController = [self getBackCollectionViewController];
    if (collectionViewController)
    {
        [collectionViewController addNewItemInCollectionViewWithPerson: newPerson];
        
        [self.navigationController popToViewController: collectionViewController
                                                                      animated: YES];
    }
    else
    {
        MainViewController* mainViewController = self.navigationController.viewControllers.firstObject;
        [mainViewController addNewRowInTabelViewWithPerson: newPerson];
        
        [self.navigationController popToRootViewControllerAnimated: YES];
    }
}

- (void) updatePersonInformationAndGoToTheMainController
{
    
    Person* editedPerson = [self getPersonWithInformationFromFields];
    
    CollectionMainViewController* collectionViewController  = [self.navigationController.viewControllers objectAtIndex:
                                                                                                       self.navigationController.viewControllers.count - 3];
    
    if ([collectionViewController isKindOfClass: [CollectionMainViewController class]])
    {
            [collectionViewController updateItemInCollectionViewWithPerson: editedPerson
                                                                                                            atIndex: self.indexEditingPerson];
        
            [self.navigationController popToViewController: collectionViewController
                                                                          animated: YES];
    }
    else
    {
        MainViewController* mainViewController = self.navigationController.viewControllers.firstObject;
        
        [mainViewController updateRowInTabelViewWithPerson: editedPerson
                                                                                        atIndex: self.indexEditingPerson];
        
        [self.navigationController popToRootViewControllerAnimated: YES];
    }
}

- (CollectionMainViewController*) getBackCollectionViewController
{
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2)
    {
        return nil;
    }
    else
    {
        if ([[self.navigationController.viewControllers objectAtIndex: numberOfViewControllers - 2]
                                                                                   isKindOfClass: [CollectionMainViewController class]])
        {
            return [self.navigationController.viewControllers objectAtIndex: numberOfViewControllers - 2];
        }
        else
        {
            return nil;
        }
    }
}

- (Person*) getPersonWithInformationFromFields
{
    Person* person = [[Person alloc] init];
    person.firstName           = self.firstNameField.text;
    person.lastName            = self.lastNameField.text;
    person.phoneNumber    = self.phoneNumberField.text;
    person.kindOfActivity     = self.kindOfActivityField.text;
    
    if (self.titleContactLogo)
    {
        person.titleContactLogo = self.titleContactLogo;
    }
    else
    {
        person.titleContactLogo = self.editingPerson.titleContactLogo;
    }
    
    return person;
}

@end
















