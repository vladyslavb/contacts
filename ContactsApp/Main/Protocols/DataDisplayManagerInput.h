//
//  DataDisplayManagerInput.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 6/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataDisplayManagerInput <NSObject>

//methods
- (NSMutableArray*) fetchTableContent;

@end
