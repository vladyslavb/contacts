//
//  Person.m
//  ContactsApp
//
//  Created by Vladyslav Bedro on 6/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "Person.h"

@implementation Person


#pragma mark - Static variables -

static NSString* firstNames[] = {
    @"Tran", @"Lenore", @"Bud", @"Fredda", @"Katrice",
    @"Clyde", @"Hildegard", @"Vernell", @"Nellie", @"Rupert",
    @"Billie", @"Tamica", @"Crystle", @"Kandi", @"Caridad",
    @"Vanetta", @"Taylor", @"Pinkie", @"Ben", @"Rosanna",
    @"Eufemia", @"Britteny", @"Ramon", @"Jacque", @"Telma",
    @"Colton", @"Monte", @"Pam", @"Tracy", @"Tresa",
    @"Willard", @"Mireille", @"Roma", @"Elise", @"Trang",
    @"Ty", @"Pierre", @"Floyd", @"Savanna", @"Arvilla",
    @"Whitney", @"Denver", @"Norbert", @"Meghan", @"Tandra",
    @"Jenise", @"Brent", @"Elenor", @"Sha", @"Jessie"
};

static NSString* lastNames[] = {
    @"Farrah", @"Laviolette", @"Heal", @"Sechrest", @"Roots",
    @"Homan", @"Starns", @"Oldham", @"Yocum", @"Mancia",
    @"Prill", @"Lush", @"Piedra", @"Castenada", @"Warnock",
    @"Vanderlinden", @"Simms", @"Gilroy", @"Brann", @"Bodden",
    @"Lenz", @"Gildersleeve", @"Wimbish", @"Bello", @"Beachy",
    @"Jurado", @"William", @"Beaupre", @"Dyal", @"Doiron",
    @"Plourde", @"Bator", @"Krause", @"Odriscoll", @"Corby",
    @"Waltman", @"Michaud", @"Kobayashi", @"Sherrick", @"Woolfolk",
    @"Holladay", @"Hornback", @"Moler", @"Bowles", @"Libbey",
    @"Spano", @"Folson", @"Arguelles", @"Burke", @"Rook"
};

static NSString* kindsOfActivity[] = {
    @"3D printing", @"Acting", @"Amateur radio", @"Aquascaping", @"Baking",
    @"Baton twirling", @"Board/tabletop games", @"Book restoration", @"Cabaret", @"Calligraphy",
    @"Candle making", @"Coffee roasting", @"Coloring", @"Computer programming", @"Computer science",
    @"Cooking", @"Cosplaying", @"Couponing", @"Creative writing", @"Crocheting",
    @"Cross-stitch", @"Crossword puzzles", @"Cryptography", @"Dance", @"Digital arts",
    @"Do it yourself", @"Drama", @"Drawing", @"Electronics", @"Embroidery",
    @"Fantasy sports", @"Fashion", @"Fishkeeping", @"Flower arranging", @"Foreign language learning",
    @"Gaming", @"Genealogy", @"Glassblowing", @"Gunsmithing", @"Herp keeping",
    @"Homebrewing", @"Hydroponics", @"Ice skating", @"Jewelry making", @"Jigsaw puzzles",
    @"Juggling", @"Knife making", @"Knitting", @"Kombucha brewing", @"Lace"
};

static NSString* phoneNumbers[] = {
    @"+ 38 (097) 111-11-11", @"+ 38 (097) 111-11-21", @"+ 38 (097) 111-31-11", @"+ 38 (097) 111-00-00", @"+ 38 (097) 111-99-11",
    @"+ 38 (097) 111-11-12", @"+ 38 (097) 111-11-31", @"+ 38 (097) 111-32-11", @"+ 38 (097) 222-11-11", @"+ 38 (097) 111-88-11",
    @"+ 38 (097) 111-11-13", @"+ 38 (097) 111-11-41", @"+ 38 (097) 111-33-11", @"+ 38 (097) 333-11-11", @"+ 38 (097) 111-77-11",
    @"+ 38 (097) 111-11-14", @"+ 38 (097) 111-11-51", @"+ 38 (097) 111-34-11", @"+ 38 (097) 444-11-11", @"+ 38 (097) 111-66-11",
    @"+ 38 (097) 111-11-15", @"+ 38 (097) 111-11-61", @"+ 38 (097) 111-35-11", @"+ 38 (097) 555-11-11", @"+ 38 (097) 111-55-11",
    @"+ 38 (097) 111-11-16", @"+ 38 (097) 111-11-71", @"+ 38 (097) 111-36-11", @"+ 38 (097) 666-11-11", @"+ 38 (097) 111-44-11",
    @"+ 38 (097) 111-11-17", @"+ 38 (097) 111-11-81", @"+ 38 (097) 111-37-11", @"+ 38 (097) 777-11-11", @"+ 38 (097) 111-33-11",
    @"+ 38 (097) 111-11-18", @"+ 38 (097) 111-11-91", @"+ 38 (097) 111-38-11", @"+ 38 (097) 888-11-11", @"+ 38 (097) 111-22-11",
    @"+ 38 (097) 111-11-19", @"+ 38 (097) 111-11-01", @"+ 38 (097) 111-39-11", @"+ 38 (097) 999-11-11", @"+ 38 (097) 111-79-11",
    @"+ 38 (097) 111-11-10", @"+ 38 (097) 111-11-22", @"+ 38 (097) 111-40-11", @"+ 38 (097) 000-11-11", @"+ 38 (097) 111-78-11"
};

static int supportCount = 50;


#pragma mark - Public methods -

+ (Person*) randomPerson
{
    Person* person = [[Person alloc] init];
    
    person.firstName = firstNames[arc4random() % supportCount];
    person.lastName = lastNames[arc4random() % supportCount];
    person.kindOfActivity = kindsOfActivity[arc4random() % supportCount];
    person.phoneNumber = phoneNumbers[arc4random() % supportCount];
    person.titleContactLogo = [NSString stringWithFormat:@"Image-%d", ((arc4random() % 800) + 100) / 100];
    
    return person;
}

@end









