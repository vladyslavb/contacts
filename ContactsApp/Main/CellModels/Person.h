//
//  Person.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 6/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Person : NSObject

//properties
@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;
@property (strong, nonatomic) NSString* kindOfActivity;
@property (strong, nonatomic) NSString* phoneNumber;
@property (strong, nonatomic) NSString* titleContactLogo;

//methods
+ (Person*) randomPerson;

@end
