//
//  MainControllerModel.m
//  ContactsApp
//
//  Created by Vladyslav Bedro on 6/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainControllerModel.h"
#import "Person.h"

@implementation MainControllerModel


#pragma mark - Display manager protocol methods -

- (NSMutableArray*) fetchTableContent
{
        NSMutableArray* randomPersonsArray = [NSMutableArray array];
        
        for (int j = 0; j < ((arc4random() % 11) + 15); j++)
        {
            Person* person = [Person randomPerson];
            [randomPersonsArray addObject: person];
        }
    
    return randomPersonsArray;
}

@end
