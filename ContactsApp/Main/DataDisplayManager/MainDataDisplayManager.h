//
//  MainDataDisplayManager.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 6/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

//protocols
#import "DataDisplayManagerInput.h"
#import "DataDisplayManagerOutput.h"

@interface MainDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

//properties
@property (strong, nonatomic) NSMutableArray* personsArray;

//methods
- (instancetype) initWithDataSource: (id<DataDisplayManagerInput>)    dataSource
                                          withOutput: (id<DataDisplayManagerOutput>) output;

@end
