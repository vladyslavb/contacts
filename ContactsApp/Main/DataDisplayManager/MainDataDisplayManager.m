//
//  MainDataDisplayManager.m
//  ContactsApp
//
//  Created by Vladyslav Bedro on 6/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainDataDisplayManager.h"

//classes
#import "Person.h"
#import "PersonCell.h"

//static variables
static NSString* kPersonIdentifier = @"PersonCell_ID";

@interface MainDataDisplayManager()

//properties
@property (weak, nonatomic) id<DataDisplayManagerInput>    dataSource;
@property (weak, nonatomic) id<DataDisplayManagerOutput> output;

@end

@implementation MainDataDisplayManager


#pragma mark - Initialization -

- (instancetype) initWithDataSource: (id<DataDisplayManagerInput>)     dataSource
                                          withOutput: (id<DataDisplayManagerOutput>) output
{
    if (self == [super init])
    {
        self.dataSource = dataSource;
        self.output         = output;
    }
    
    return self;
}


#pragma mark - Properties -

- (NSMutableArray*) personsArray
{
    if (_personsArray == nil)
    {
        _personsArray = [self.dataSource fetchTableContent];
    }
    
    return _personsArray;
}


#pragma mark - UITableViewDataSource methods -

- (NSInteger) numberOfSectionsInTableView: (UITableView*) tableView
{
    return 1;
}

- (NSInteger)         tableView: (UITableView*) tableView
      numberOfRowsInSection: (NSInteger) section
{
    return [self.personsArray count];
}

- (UITableViewCell*) tableView: (UITableView*) tableView
             cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    PersonCell* cell = [tableView dequeueReusableCellWithIdentifier: kPersonIdentifier
                                                                                           forIndexPath: indexPath];
    
        [cell fillContentToIndexPath: indexPath
                        withPersonsArray: self.personsArray];
        
        return cell;
}

- (BOOL)                   tableView: (UITableView*) tableView
         canMoveRowAtIndexPath: (NSIndexPath*) indexPath
{
    return YES;
}

- (void)            tableView: (UITableView*)  tableView
     moveRowAtIndexPath: (NSIndexPath*) sourceIndexPath
                     toIndexPath: (NSIndexPath*) destinationIndexPath
{
    if (sourceIndexPath.section == destinationIndexPath.section)
    {
        Person* movingPerson = [self.personsArray objectAtIndex: sourceIndexPath.row];
    
        [self.personsArray removeObjectAtIndex: sourceIndexPath.row];
        [self.personsArray insertObject: movingPerson
                                                atIndex: destinationIndexPath.row];
    }
}

- (void)          tableView: (UITableView*) tableView
      commitEditingStyle: (UITableViewCellEditingStyle) editingStyle
       forRowAtIndexPath: (NSIndexPath*) indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        Person* deletingPerson = [self.personsArray objectAtIndex: indexPath.row];
        [self.personsArray removeObject: deletingPerson];
        
        [tableView beginUpdates];

        [tableView deleteRowsAtIndexPaths: @[indexPath]
                                     withRowAnimation: UITableViewRowAnimationRight];

        [tableView endUpdates];
    }
}


#pragma mark - UITableViewDelegate methods -

- (NSString*)                                                    tableView: (UITableView*) tableView
   titleForDeleteConfirmationButtonForRowAtIndexPath: (NSIndexPath*) indexPath
{
    return @"Remove";
}

- (BOOL)                                      tableView: (UITableView*)  tableView
shouldIndentWhileEditingRowAtIndexPath: (NSIndexPath*) indexPath
{
    return NO;
}

- (void)                      tableView: (UITableView*)  tableView
        didSelectRowAtIndexPath: (NSIndexPath*) indexPath
{
    if ([self.output respondsToSelector: @selector(didSelectItemWithPerson:)])
    {
        [tableView deselectRowAtIndexPath: indexPath
                                                    animated: YES];
        
        [self.output didSelectItemWithPerson: self.personsArray[indexPath.row]];
    }
}

- (BOOL)                                   tableView: (UITableView*) tableView
    shouldShowMenuForRowAtIndexPath: (NSIndexPath*) indexPath
{
        return YES;
}

- (BOOL) tableView: (UITableView*) tableView
  canPerformAction: (SEL) action
 forRowAtIndexPath: (NSIndexPath*) indexPath
              withSender: (id) sender
{
    if (action == @selector(copy:))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)   tableView: (UITableView*) tableView
        performAction: (SEL) action
forRowAtIndexPath: (NSIndexPath*) indexPath
             withSender: (id) sender
{
    if (action == @selector(copy:))
    {
        PersonCell* cell = [tableView cellForRowAtIndexPath: indexPath];
        UIPasteboard* pasteBoard = [UIPasteboard generalPasteboard];
        [pasteBoard setString: cell.firstAndLastNameLabel.text];
    }
}

@end
