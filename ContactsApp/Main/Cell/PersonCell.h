//
//  PersonCell.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 6/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonCell : UITableViewCell

//outlets
@property (weak, nonatomic) IBOutlet UILabel*          firstAndLastNameLabel;

//methods
- (void) fillContentToIndexPath: (NSIndexPath*) indexPath
                      withPersonsArray: (NSMutableArray*) personsArray;

@end
