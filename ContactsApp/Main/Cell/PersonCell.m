//
//  PersonCell.m
//  ContactsApp
//
//  Created by Vladyslav Bedro on 6/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "PersonCell.h"
#import "Person.h"

@interface PersonCell()

//outlets
@property (weak, nonatomic) IBOutlet UIImageView* contactImage;
@property (weak, nonatomic) IBOutlet UILabel*          kindOfActivityLabel;

@end

@implementation PersonCell

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self configureImageView];
}

- (void) setSelected: (BOOL) selected
                  animated: (BOOL) animated
{
    [super setSelected: selected
                     animated: animated];
}


#pragma mark - Public methods -

- (void) fillContentToIndexPath: (NSIndexPath*) indexPath
                      withPersonsArray: (NSMutableArray*) personsArray
{
    Person* person = [personsArray objectAtIndex: indexPath.row];
    
    self.firstAndLastNameLabel.text = [NSString stringWithFormat: @"%@ %@",
                                                                                                              person.firstName,
                                                                                                              person.lastName];
    
    self.kindOfActivityLabel.text = [NSString stringWithFormat: @"%@", person.kindOfActivity];
    NSString* contactImageName = [NSString stringWithFormat: @"%@.png", person.titleContactLogo];
    self.contactImage.image = [UIImage imageNamed: contactImageName];
}


#pragma mark - Private methods -

- (void) configureImageView
{
    self.contactImage.layer.backgroundColor = [[UIColor clearColor] CGColor];
    self.contactImage.layer.cornerRadius        =  self.contactImage.frame.size.height / 2;
    self.contactImage.layer.masksToBounds   = YES;
    self.contactImage.layer.borderColor          = [[UIColor whiteColor] CGColor];
    self.contactImage.layer.borderWidth         = 2.0;
    self.contactImage.clipsToBounds               = YES;
}

@end





















