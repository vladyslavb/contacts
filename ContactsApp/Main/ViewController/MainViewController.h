//
//  MainViewController.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 6/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainDataDisplayManager.h"
#import "MainControllerModel.h"

//protocols
#import "DataDisplayManagerOutput.h"

@interface MainViewController : UIViewController <DataDisplayManagerOutput>

//properties
@property (weak, nonatomic) id<DataDisplayManagerOutput> output;

//methods
- (void) fillContentWithPersonsArray: (NSMutableArray*) personsArray;
- (void) addNewRowInTabelViewWithPerson: (Person*) newPerson;
- (void) updateRowInTabelViewWithPerson:   (Person*) editedPerson
                                                           atIndex:  (NSInteger) indexEditedPerson;

- (void) reloadViewData;

@end
