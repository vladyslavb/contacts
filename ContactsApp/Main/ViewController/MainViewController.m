//
//  MainViewController.m
//  ContactsApp
//
//  Created by Vladyslav Bedro on 6/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainViewController.h"

//Classes
#import "Person.h"
#import "InformationViewController.h"
#import "AddContactViewController.h"
#import "CollectionMainViewController.h"


@interface MainViewController () 

//outlets
@property (weak, nonatomic) IBOutlet UITableView* mainTableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* editModeButton;

//properties
@property (strong, nonatomic) MainDataDisplayManager* displayManager;
@property (strong, nonatomic) MainControllerModel*         model;

@property (strong, nonatomic) UIRefreshControl* refreshControl;

@end

@implementation MainViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self bindingUI];
    [self configurateMainTableView];
    [self createRefreshControl];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Properties -

- (MainDataDisplayManager*) displayManager
{
    if (_displayManager == nil)
    {
        _displayManager = [[MainDataDisplayManager alloc] initWithDataSource: self.model
                                                                                                              withOutput: self];
    }
    
    return _displayManager;
}

- (MainControllerModel*) model
{
    if (_model == nil)
    {
        _model = [MainControllerModel new];
    }
    
    return _model;
}


#pragma mark - Display manager output methods -

- (void) didSelectItemWithPerson: (Person*) person
{
    UIStoryboard* informationStoryboard = [UIStoryboard storyboardWithName: @"Information"
                                                                                                                       bundle: nil];
    InformationViewController* informationVC = [informationStoryboard instantiateViewControllerWithIdentifier: @"InformationVC"];
    [informationVC fillInformationPerson: person
                                         inPersonsAray: self.displayManager.personsArray];
    
    [self.navigationController pushViewController: informationVC
                                                                animated: YES];
}


#pragma mark - Public methods -

- (void) reloadViewData // remark: (?)
{
    [self.mainTableView reloadData];
}

- (void) fillContentWithPersonsArray: (NSMutableArray*) personsArray
{
    self.displayManager.personsArray = personsArray;
}

- (void) addNewRowInTabelViewWithPerson: (Person*) newPerson
{
    NSMutableArray* tempArray = nil;
    
    if (self.displayManager.personsArray)
    {
        tempArray = [NSMutableArray arrayWithArray: self.displayManager.personsArray];
    }
    else
    {
        tempArray = [NSMutableArray array];
    }
    
    if (!newPerson.titleContactLogo)
    {
        newPerson.titleContactLogo = @"defaultContactLogo";
    }
    
     NSInteger newPersonIndex = 0;
    
    [tempArray insertObject: newPerson
                                atIndex: newPersonIndex];
    
    self.displayManager.personsArray = tempArray;
    
    [self.mainTableView beginUpdates];
    
    NSIndexPath* newIndexPath = [NSIndexPath indexPathForItem: newPersonIndex
                                                                                              inSection: 0];
    
    [self.mainTableView insertRowsAtIndexPaths: @[newIndexPath]
                                                withRowAnimation: UITableViewRowAnimationLeft];
    
    [self.mainTableView endUpdates];
    
    self.mainTableView.userInteractionEnabled = NO;
    dispatch_after (dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(![self.mainTableView isUserInteractionEnabled])
        {
            self.mainTableView.userInteractionEnabled = YES;
        }
    });
    
}

- (void) updateRowInTabelViewWithPerson:  (Person*)     editedPerson
                                                           atIndex:  (NSInteger) indexEditedPerson;
{
    NSMutableArray* tempArray = nil;
    
    if (self.displayManager.personsArray)
    {
        tempArray = [NSMutableArray arrayWithArray: self.displayManager.personsArray];
    }
    else
    {
        tempArray = [NSMutableArray array];
    }
    
    if (!editedPerson.titleContactLogo)
    {
        editedPerson.titleContactLogo = @"defaultContactLogo";
    }
    
    tempArray[indexEditedPerson] = editedPerson;
    
    self.displayManager.personsArray = tempArray;
    
    [self.mainTableView beginUpdates];
    
    NSIndexPath* newIndexPath = [NSIndexPath indexPathForItem: indexEditedPerson
                                                                                              inSection: 0];
    
    [self.mainTableView reloadRowsAtIndexPaths: @[newIndexPath]
                                                withRowAnimation: UITableViewRowAnimationRight];
    
    [self.mainTableView endUpdates];
    
    self.mainTableView.userInteractionEnabled = NO;
    dispatch_after (dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(![self.mainTableView isUserInteractionEnabled])
        {
            self.mainTableView.userInteractionEnabled = YES;
        }
    });
}


#pragma mark - Private methods -

- (void) configurateMainTableView
{
    self.mainTableView.editing = NO;
    self.mainTableView.allowsSelectionDuringEditing = NO;
    
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"firstName"
                                                                                                              ascending: YES];
    
    [self.displayManager.personsArray sortUsingDescriptors: @[sortDescriptor]];
}

- (void) createRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.mainTableView addSubview: self.refreshControl];
    [self.refreshControl addTarget: self
                                            action: @selector(refreshTable)
                          forControlEvents: UIControlEventValueChanged];
}


#pragma mark - Actions -

- (void) refreshTable {
    // remark: will be done
  
    [self.refreshControl endRefreshing];
    [self.mainTableView reloadData];
}

- (IBAction) onAddContactPressed: (UIBarButtonItem*) sender
{
    [self performSegueWithIdentifier: @"ShowAddContactViewControllerID"
                                               sender: nil];
}

- (IBAction) onShowCollectionMainViewControllerButtonPressed: (UIBarButtonItem*) sender
{
    UIStoryboard* collectionStoryboard = [UIStoryboard storyboardWithName: @"MainCollection"
                                                                                                                     bundle:  nil];
    
    CollectionMainViewController* collectionVC = [collectionStoryboard instantiateViewControllerWithIdentifier: @"CollectionViewID"];
    
    [collectionVC fillContentWithPersonsArray: self.displayManager.personsArray];
    
    [self.navigationController pushViewController: collectionVC
                                                                animated: YES];
}

- (IBAction) onEditModeButtonPressed: (UIBarButtonItem*) sender
{
    [self.mainTableView setEditing: !self.mainTableView.editing
                                       animated: YES];
    
    if (self.mainTableView.editing)
    {
        self.editModeButton.tintColor = [UIColor blackColor];
    }
    else
    {
        self.editModeButton.tintColor = [UIColor blueColor];
    }
}


#pragma mark - UI -

- (void) bindingUI
{
    self.mainTableView.dataSource = self.displayManager;
    self.mainTableView.delegate      = self.displayManager;
    
     UINib* nibForPersonCell = [UINib nibWithNibName: @"PersonCell"
                                bundle: nil];
    
    [self.mainTableView registerNib: nibForPersonCell
                    forCellReuseIdentifier: @"PersonCell_ID"];
}

@end
