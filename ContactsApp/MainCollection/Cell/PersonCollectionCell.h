//
//  PersonCollectionCell.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/4/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonCollectionCell : UICollectionViewCell

//methods
- (void) fillContentToIndexPath: (NSIndexPath*) indexPath
                      withPersonsArray: (NSMutableArray*) personsArray;

@end
