//
//  PersonCollectionCell.m
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/4/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "PersonCollectionCell.h"
#import "Person.h"

@interface PersonCollectionCell()

// outlets
@property (weak, nonatomic) IBOutlet UIImageView* contactImage;
@property (weak, nonatomic) IBOutlet UILabel* firstNameLabel;
@property (weak, nonatomic) IBOutlet UILabel* lastNameLabel;
@property (weak, nonatomic) IBOutlet UILabel* phoneNumberLabel;

@end

@implementation PersonCollectionCell

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self configureImageView];
}


#pragma mark - Public methods -

- (void) fillContentToIndexPath: (NSIndexPath*) indexPath
                      withPersonsArray: (NSMutableArray*) personsArray
{
    Person* person = [personsArray objectAtIndex: indexPath.row];
    
    self.firstNameLabel.text        = [NSString stringWithFormat: @"%@", person.firstName];
    self.lastNameLabel.text         = [NSString  stringWithFormat: @"%@", person.lastName];
    self.phoneNumberLabel.text = [NSString  stringWithFormat: @"%@", person.phoneNumber];
    
    NSString* contactImageName = [NSString stringWithFormat: @"%@.png", person.titleContactLogo];
    self.contactImage.image = [UIImage imageNamed: contactImageName];
}


#pragma mark - Private methods -

- (void) configureImageView
{
    self.contactImage.layer.backgroundColor = [[UIColor clearColor] CGColor];
    self.contactImage.layer.cornerRadius        =  self.contactImage.frame.size.height / 2;
    self.contactImage.layer.masksToBounds   = YES;
    self.contactImage.layer.borderColor          = [[UIColor whiteColor] CGColor];
    self.contactImage.layer.borderWidth         = 4.0;
    self.contactImage.clipsToBounds               = YES;
}

@end
