//
//  MainCollectionDataDisplayManager.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/4/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

//protocols
#import "DataDisplayManagerInput.h"
#import "DataDisplayManagerOutput.h"

@interface MainCollectionDataDisplayManager : NSObject <UICollectionViewDataSource, UICollectionViewDelegate>

//properties
@property (strong, nonatomic) NSMutableArray* personsArray;

//methods
- (instancetype) initWithDataSource: (id<DataDisplayManagerInput>)    dataSource
                                          withOutput: (id<DataDisplayManagerOutput>) output;

- (void) fillContent: (NSMutableArray*) content;

@end
