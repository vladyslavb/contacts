//
//  MainCollectionDataDisplayManager.m
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/4/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainCollectionDataDisplayManager.h"

//classes
#import "Person.h"
#import "PersonCollectionCell.h"
#import "MainViewController.h"

//static variables
static NSString* kPersonIdentifier = @"PersonCollectionCell_ID";

@interface MainCollectionDataDisplayManager()

//properties
@property (weak, nonatomic) id<DataDisplayManagerInput>    dataSource;
@property (weak, nonatomic) id<DataDisplayManagerOutput> output;

@end

@implementation MainCollectionDataDisplayManager


#pragma mark - Initialization -

- (instancetype) initWithDataSource: (id<DataDisplayManagerInput>)     dataSource
                                          withOutput: (id<DataDisplayManagerOutput>)  output
{
    if (self == [super init])
    {
        self.dataSource = dataSource;
        self.output         = output;
    }
    
    return self;
}


#pragma mark - Public methods -

- (void) fillContent: (NSMutableArray*) content
{
    self.personsArray = content;
    
    [self.output reloadViewData];
}


#pragma mark - UICollectionViewDataSource methods -

- (NSInteger) numberOfSectionsInCollectionView: (UICollectionView*) collectionView
{
    return 1;
}

- (NSInteger) collectionView: (UICollectionView*) collectionView
      numberOfItemsInSection: (NSInteger) section
{
        return [self.personsArray count];
}

- (UICollectionViewCell*) collectionView: (UICollectionView*) collectionView
                            cellForItemAtIndexPath: (NSIndexPath*) indexPath
{
    PersonCollectionCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier: kPersonIdentifier
                                                                                                                              forIndexPath: indexPath];
    
    [cell fillContentToIndexPath: indexPath
                     withPersonsArray: self.personsArray];
    
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 20;
    
    return cell;
}

- (BOOL)      collectionView: (UICollectionView*) collectionView
   canMoveItemAtIndexPath: (NSIndexPath*) indexPath
{
    return YES;
}

- (void)    collectionView: (UICollectionView*) collectionView
    moveItemAtIndexPath: (NSIndexPath*) sourceIndexPath
                    toIndexPath: (NSIndexPath*) destinationIndexPath
{
    if (sourceIndexPath.section == destinationIndexPath.section)
    {
        Person* movingPerson = [self.personsArray objectAtIndex: sourceIndexPath.item];
        
        [self.personsArray removeObjectAtIndex: sourceIndexPath.item];
        [self.personsArray insertObject: movingPerson
                                                atIndex: destinationIndexPath.item];
    }
}


#pragma mark - UICollectionViewDelegate methods -

- (void)               collectionView: (UICollectionView*) collectionView
    didHighlightItemAtIndexPath: (NSIndexPath*) indexPath
{
    PersonCollectionCell* cell = (PersonCollectionCell*) [collectionView cellForItemAtIndexPath: indexPath];
    UIColor* cellColor = cell.backgroundColor;
    
    const NSTimeInterval kAnimationDuration = 1.0;
    [UIView animateWithDuration: kAnimationDuration
                                   animations: ^{
                                                            cell.backgroundColor = [UIColor lightGrayColor];
                                                         }
                                  completion: ^(BOOL finished)
                                                        {
                                                            [UIView animateWithDuration: kAnimationDuration
                                                                                           animations: ^{
                                                                                                                    cell.backgroundColor = cellColor;
                                                                                                                }];
                                                        }];
}

-           (void) collectionView: (UICollectionView*) collectionView
    didSelectItemAtIndexPath: (NSIndexPath*) indexPath
{
    if ([self.output respondsToSelector: @selector(didSelectItemWithPerson:)])
    {
        [self.output didSelectItemWithPerson: self.personsArray[indexPath.row]];
    }
    
    [collectionView deselectItemAtIndexPath: indexPath
                                                         animated: YES];
}

@end























