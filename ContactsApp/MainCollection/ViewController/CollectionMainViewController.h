//
//  CollectionMainViewController.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/4/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainCollectionDataDisplayManager.h"

//protocols
#import "DataDisplayManagerOutput.h"

@interface CollectionMainViewController : UIViewController <DataDisplayManagerOutput>

//properties
@property (weak, nonatomic) id<DataDisplayManagerOutput> output;

//methods
- (void) reloadViewData;

- (void) fillContentWithPersonsArray: (NSMutableArray*) personsArray;
- (void) addNewItemInCollectionViewWithPerson: (Person*) newPerson;
- (void) updateItemInCollectionViewWithPerson: (Person*) editedPerson
                                                                    atIndex: (NSInteger) indexEditedPerson;

@end


