//
//  CollectionMainViewController.m
//  ContactsApp
//
//  Created by Vladyslav Bedro on 6/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "CollectionMainViewController.h"

//Classes
#import "Person.h"
#import "MainDataDisplayManager.h"
#import "MainControllerModel.h"
#import "InformationViewController.h"
#import "AddContactViewController.h"
#import "MainViewController.h"

@interface CollectionMainViewController ()

//outlets
@property (weak, nonatomic) IBOutlet UICollectionView *mainCollectionView;

//properties
@property (strong, nonatomic) MainCollectionDataDisplayManager* displayManager;
@property (strong, nonatomic) MainControllerModel* model;
@property (assign, nonatomic) NSIndexPath* indexPathDeletingPerson;

@property (strong, nonatomic) UIRefreshControl* refreshControl;

@end

@implementation CollectionMainViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self bindingUI];
    [self createRefreshControl];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Properties -

- (MainCollectionDataDisplayManager*) displayManager
{
    if (_displayManager == nil)
    {
        _displayManager = [[MainCollectionDataDisplayManager alloc] initWithDataSource: self.model
                                                                                                                                withOutput: self];
    }
    
    return _displayManager;
}


#pragma mark - Display manager output methods -

- (void) didSelectItemWithPerson: (Person*) person
{
    UIStoryboard* informationStoryboard = [UIStoryboard storyboardWithName:  @"Information"
                                                                                                                        bundle:  nil];
    
    InformationViewController* informationViewController = [informationStoryboard instantiateViewControllerWithIdentifier:
                                                                                                                                          @"InformationVC"];
    [informationViewController fillInformationPerson: person
                                                            inPersonsAray: self.displayManager.personsArray];
    
    [self.navigationController pushViewController: informationViewController
                                                                animated: YES];
}


#pragma mark - Public methods -

- (void) reloadViewData
{
    [self.mainCollectionView reloadData];
}

- (void) fillContentWithPersonsArray: (NSMutableArray*) personsArray
{
    self.displayManager.personsArray = personsArray;
}

- (void) addNewItemInCollectionViewWithPerson: (Person*) newPerson
{
    NSMutableArray* tempArray = nil;
    
    if (self.displayManager.personsArray)
    {
        tempArray = [NSMutableArray arrayWithArray: self.displayManager.personsArray];
    }
    else
    {
        tempArray = [NSMutableArray array];
    }
    
    if (!newPerson.titleContactLogo)
    {
        newPerson.titleContactLogo = @"defaultContactLogo";
    }
    
    NSInteger newPersonIndex = 0;
    
    [tempArray insertObject: newPerson
                                atIndex: newPersonIndex];
    
    self.displayManager.personsArray = tempArray;
    
    NSIndexPath* newIndexPath = [NSIndexPath indexPathForItem: newPersonIndex
                                                                                             inSection: 0];
    
    [self.mainCollectionView insertItemsAtIndexPaths: @[newIndexPath]];
    [self.mainCollectionView reloadData];
    
    self.mainCollectionView.userInteractionEnabled = NO;
    dispatch_after (dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(![self.mainCollectionView isUserInteractionEnabled])
        {
            self.mainCollectionView.userInteractionEnabled = YES;
        }
    });
    
}

- (void) updateItemInCollectionViewWithPerson:  (Person*)     editedPerson
                                                                    atIndex:  (NSInteger) indexEditedPerson;
{
    NSMutableArray* tempArray = nil;
    
    if (self.displayManager.personsArray)
    {
        tempArray = [NSMutableArray arrayWithArray: self.displayManager.personsArray];
    }
    else
    {
        tempArray = [NSMutableArray array];
    }
    
    if (!editedPerson.titleContactLogo)
    {
        editedPerson.titleContactLogo = @"defaultContactLogo";
    }
    
    tempArray[indexEditedPerson] = editedPerson;
    
    self.displayManager.personsArray = tempArray;
    
    NSIndexPath* newIndexPath = [NSIndexPath indexPathForItem: indexEditedPerson
                                                                                             inSection: 0];

    [self.mainCollectionView reloadItemsAtIndexPaths: @[newIndexPath]];
    
    [self.mainCollectionView reloadData];
    
    self.mainCollectionView.userInteractionEnabled = NO;
    dispatch_after (dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(![self.mainCollectionView isUserInteractionEnabled])
        {
            self.mainCollectionView.userInteractionEnabled = YES;
        }
    });
}


#pragma mark - Private methods -

- (void) createRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.mainCollectionView addSubview: self.refreshControl];
    [self.refreshControl addTarget: self
                                            action: @selector(refreshTable)
                          forControlEvents: UIControlEventValueChanged];
}

- (void) createDeletingAlert
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle: @"Deleting"
                                                                                                                      message: @"Are you sure you want to delete this contact?"
                                                                                                             preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButtonAction = [UIAlertAction actionWithTitle: @"Yes"
                                                                                                      style: UIAlertActionStyleDefault
                                                                                                 handler: ^(UIAlertAction * _Nonnull action)
                                                            {
                                                                
                                                                Person* deletingPerson = [self.displayManager.personsArray
                                                                                                            objectAtIndex: self.indexPathDeletingPerson.item];
                                                                
                                                                [self.displayManager.personsArray removeObject: deletingPerson];
                                                                [self.mainCollectionView deleteItemsAtIndexPaths: @[self.indexPathDeletingPerson]];
                                                            }];
    
    UIAlertAction* noButtonAction = [UIAlertAction actionWithTitle: @"No"
                                                             style: UIAlertActionStyleDefault
                                                           handler: ^(UIAlertAction * _Nonnull action) {
                                                            // "No-answer logic"
                                                           }];

    [alertController addAction: yesButtonAction];
    [alertController addAction: noButtonAction];
    
    [self presentViewController: alertController
                                  animated: YES
                              completion: nil];
}


#pragma mark - Actions -

- (void) refreshTable
{
    // remark: will be done
    
    [self.refreshControl endRefreshing];
    [self.mainCollectionView reloadData];
}

- (IBAction) onAddContactPressed: (UIBarButtonItem*) sender
{
    [self performSegueWithIdentifier: @"ShowAddContactViewControllerID"
                                                sender: nil];
}

- (IBAction) onShowMainViewControllerButtonPressed: (UIBarButtonItem*) sender
{
    UIStoryboard* mainTableStoryboard = [UIStoryboard storyboardWithName: @"Main"
                                                                                                                    bundle: nil];
    
    MainViewController* mainViewController = [mainTableStoryboard instantiateViewControllerWithIdentifier: @"MainTableViewID"];
    
    [mainViewController fillContentWithPersonsArray: self.displayManager.personsArray];
    
    [self.navigationController pushViewController: mainViewController
                                                                animated: YES];
}


#pragma mark - Gestures -

- (IBAction) didLongPressCellToDelete: (UILongPressGestureRecognizer*) gesture
{
    CGPoint tapLocation = [gesture locationInView: self.mainCollectionView];
    NSIndexPath *indexPath = [self.mainCollectionView indexPathForItemAtPoint: tapLocation];
    if (indexPath && gesture.state == UIGestureRecognizerStateBegan)
    {
        self.indexPathDeletingPerson = indexPath;
        [self createDeletingAlert];
    }
}

- (IBAction) handleLongPress: (UILongPressGestureRecognizer*) gestureRecognizer
{
    switch (gestureRecognizer.state)
    {
        case UIGestureRecognizerStateBegan:
        {
            NSIndexPath *selectedIndexPath = [self.mainCollectionView indexPathForItemAtPoint:
                                                                        [gestureRecognizer locationInView: self.mainCollectionView]];
            if(selectedIndexPath == nil)
            {
             break;
            }
            
            [self.mainCollectionView beginInteractiveMovementForItemAtIndexPath: selectedIndexPath];
            break;
        }
        case UIGestureRecognizerStateChanged:
        {
            [self.mainCollectionView updateInteractiveMovementTargetPosition:
            [gestureRecognizer locationInView: gestureRecognizer.view]];
            break;
        }
        case UIGestureRecognizerStateEnded:
        {
            [self.mainCollectionView endInteractiveMovement];
            break;
        }
        default:
        {
            [self.mainCollectionView cancelInteractiveMovement];
            break;
        }
    }
}


#pragma mark - UI -

- (void) bindingUI
{
    self.mainCollectionView.dataSource = self.displayManager;
    self.mainCollectionView.delegate      = self.displayManager;
    [self.mainCollectionView registerNib: [UINib nibWithNibName: @"PersonCollectionCell"
                                                                                               bundle: nil]
                                                                forCellWithReuseIdentifier: @"PersonCollectionCell_ID"];
    [self.displayManager fillContent: self.displayManager.personsArray];
}


@end
