//
//  InformationViewController.m
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/2/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

//Class
#import "InformationViewController.h"
#import "AddContactViewController.h"
#import "MainViewController.h"
#import "Person.h"

@interface InformationViewController ()

//outlets
@property (weak, nonatomic) IBOutlet UIImageView* informationContactLogo;
@property (weak, nonatomic) IBOutlet UILabel* informationFirstNameLabel;
@property (weak, nonatomic) IBOutlet UILabel* informationLastNameLabel;
@property (weak, nonatomic) IBOutlet UILabel* informationPhoneNumberLabel;

//properties
@property (weak, nonatomic) Person* selectedPerson;
@property (strong, nonatomic) NSMutableArray* personsArray;

@end

@implementation InformationViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self configureImageView];
    [self configureInformation];
}


#pragma mark - Actions -

- (IBAction) onEditButtonPressed: (UIButton*) sender
{
    UIStoryboard* addContactStoryboard = [UIStoryboard storyboardWithName: @"AddContact"
                                                                                                                        bundle: nil];
    
    AddContactViewController* addContactViewController = [addContactStoryboard
                                                                                                    instantiateViewControllerWithIdentifier: @"AddContactStoryboardID"];
    
    [addContactViewController fillInformationForPerson: self.selectedPerson
                                                                    inPersonArray: self.personsArray];
    
    [self.navigationController pushViewController: addContactViewController
                                                                animated: YES];
}

- (IBAction) onCallButtonPressed: (UIButton*) sender
{
    NSString* phoneNumber = [@"tel://" stringByAppendingString: self.selectedPerson.phoneNumber];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: phoneNumber]
                                                                 options: @{}
                                              completionHandler: nil];
}


#pragma mark - Public methods -

- (void) fillInformationPerson: (Person*) person
                        inPersonsAray: (NSMutableArray*) personsArray
{
    self.selectedPerson = person;
    self.personsArray = personsArray;
}

#pragma mark - Private methods -

- (void) configureImageView
{
    self.informationContactLogo.layer.backgroundColor = [[UIColor clearColor] CGColor];
    self.informationContactLogo.layer.cornerRadius =  self.informationContactLogo.frame.size.height / 2;
    self.informationContactLogo.layer.masksToBounds = YES;
    self.informationContactLogo.layer.borderColor=[[UIColor blackColor] CGColor];
    self.informationContactLogo.layer.borderWidth = 2.0;
    self.informationContactLogo.clipsToBounds = YES;
}

- (void) configureInformation
{
    NSString* contactImageName = [NSString stringWithFormat: @"%@.png", self.selectedPerson.titleContactLogo];
    self.informationContactLogo.image         = [UIImage imageNamed: contactImageName];
    self.informationFirstNameLabel.text        = self.selectedPerson.firstName;
    self.informationLastNameLabel.text        = self.selectedPerson.lastName;
    self.informationPhoneNumberLabel.text = self.selectedPerson.phoneNumber;
}

@end















