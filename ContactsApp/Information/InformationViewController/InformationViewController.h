//
//  InformationViewController.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/2/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"

@interface InformationViewController : UIViewController

- (void) fillInformationPerson: (Person*) person
                        inPersonsAray: (NSMutableArray*) personsArray;

@end
