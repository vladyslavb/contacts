//
//  AvatarsDisplayManagerOutput.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/9/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AvatarsDisplayManagerOutput <NSObject>

//methods
- (void) didSelectAvatarWithIndexPath: (NSIndexPath*) indexPath;

@end
