//
//  AvatarViewCell.m
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/5/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AvatarViewCell.h"

@interface AvatarViewCell()

// outlets
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;

@end

@implementation AvatarViewCell

- (void) awakeFromNib
{
    [super awakeFromNib];
}

- (void) fillContentToIndexPath: (NSIndexPath*) indexPath
{
    // remark: will be rewrite
    NSString* avatarImageName = [NSString stringWithFormat: @"Image-%ld", indexPath.row + 1];
    self.avatarImage.image = [UIImage imageNamed: avatarImageName];
}

@end

