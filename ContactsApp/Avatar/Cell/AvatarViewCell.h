//
//  AvatarViewCell.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/5/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AvatarViewCell : UICollectionViewCell

//methods
- (void) fillContentToIndexPath: (NSIndexPath*) indexPath;

@end
