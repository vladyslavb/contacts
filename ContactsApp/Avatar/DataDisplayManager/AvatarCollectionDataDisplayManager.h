//
//  AvatarCollectionDataDisplayManager.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/9/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

//protocols
#import "AvatarsDisplayManagerOutput.h"

@interface AvatarCollectionDataDisplayManager : NSObject <UICollectionViewDataSource, UICollectionViewDelegate>

//methods
- (instancetype) initWithOutput: (id<AvatarsDisplayManagerOutput>) output;

@end
