//
//  AvatarCollectionDataDisplayManager.m
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/9/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AvatarCollectionDataDisplayManager.h"
#import "AvatarsDisplayManagerOutput.h"
#import "AvatarViewCell.h"

//static variables
static NSString* kPersonIdentifier = @"AvatarCollectionCell_ID";

@interface AvatarCollectionDataDisplayManager()

//properties
@property (weak, nonatomic) id<AvatarsDisplayManagerOutput> output;

@end

@implementation AvatarCollectionDataDisplayManager


#pragma mark - Initialization -

- (instancetype) initWithOutput: (id<AvatarsDisplayManagerOutput>)  output
{
    if (self == [super init])
    {
        self.output = output;
    }
    
    return self;
}

#pragma mark - UICollectionViewDataSource methods -

- (NSInteger) numberOfSectionsInCollectionView: (UICollectionView*) collectionView
{
    return 1;
}

- (NSInteger) collectionView: (UICollectionView*) collectionView
      numberOfItemsInSection: (NSInteger) section
{
    // remark: will be rewrite
    return 8;
}

- (UICollectionViewCell*) collectionView: (UICollectionView*) collectionView
                            cellForItemAtIndexPath: (NSIndexPath*) indexPath
{
    AvatarViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier: kPersonIdentifier
                                                                                                                    forIndexPath: indexPath];
    [cell fillContentToIndexPath: indexPath];
    
    return cell;
}


#pragma mark - UICollectionViewDelegate methods -

- (void)               collectionView: (UICollectionView*) collectionView
    didHighlightItemAtIndexPath: (NSIndexPath*) indexPath
{
    AvatarViewCell* cell = (AvatarViewCell*) [collectionView cellForItemAtIndexPath: indexPath];
    UIColor* cellColor = cell.backgroundColor;
    
    const NSTimeInterval kAnimationDuration = 1.0;
    [UIView animateWithDuration: kAnimationDuration
                     animations: ^{
                         cell.backgroundColor = [UIColor lightGrayColor];
                     }
                     completion: ^(BOOL finished)
                     {
                         [UIView animateWithDuration: kAnimationDuration
                          animations:
                          ^{
                              cell.backgroundColor = cellColor;
                          }];
                      }];
}

-           (void) collectionView: (UICollectionView*) collectionView
    didSelectItemAtIndexPath: (NSIndexPath*) indexPath
{
    if ([self.output respondsToSelector: @selector(didSelectAvatarWithIndexPath:)])
    {
        [self.output didSelectAvatarWithIndexPath: indexPath];
    }
}

@end
