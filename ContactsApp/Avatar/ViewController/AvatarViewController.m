//
//  AvatarViewController.m
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/5/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AvatarViewController.h"
#import "AvatarCollectionDataDisplayManager.h"
#import "AddContactViewController.h"

@interface AvatarViewController ()

//outlets
@property (weak, nonatomic) IBOutlet UICollectionView* avatarCollectionView;

//properties
@property (strong, nonatomic) AvatarCollectionDataDisplayManager* displayManager;

@end

@implementation AvatarViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self bindingUI];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Properties -

- (AvatarCollectionDataDisplayManager*) displayManager
{
    if (_displayManager == nil)
    {
        _displayManager = [[AvatarCollectionDataDisplayManager alloc] initWithOutput: self];
    }
    
    return _displayManager;
}

#pragma mark - Display manager output methods -

- (void) didSelectAvatarWithIndexPath: (NSIndexPath*) indexPath
{
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    AddContactViewController* addContactViewController = [self.navigationController.viewControllers
                                                                                                     objectAtIndex:  numberOfViewControllers - 2];
    
    NSString* titleContactLogo = [NSString stringWithFormat: @"Image-%ld", (long) indexPath.row + 1];
    
    [addContactViewController fillContentWithTitleContactLogo: titleContactLogo];
    
    [self.navigationController popToViewController: addContactViewController
                                                                  animated: YES];
}


#pragma mark - UI -

- (void) bindingUI
{
    self.avatarCollectionView.dataSource = self.displayManager;
    self.avatarCollectionView.delegate      = self.displayManager;
    [self.avatarCollectionView registerNib: [UINib nibWithNibName: @"AvatarViewCell"
                                                       bundle: nil]
                       forCellWithReuseIdentifier: @"AvatarCollectionCell_ID"];
}


@end
