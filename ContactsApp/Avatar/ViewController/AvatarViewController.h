//
//  AvatarViewController.h
//  ContactsApp
//
//  Created by Vladyslav Bedro on 7/5/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarsDisplayManagerOutput.h"

@interface AvatarViewController : UIViewController <AvatarsDisplayManagerOutput>

//properties
@property (weak, nonatomic) id<AvatarsDisplayManagerOutput> output;

@end
